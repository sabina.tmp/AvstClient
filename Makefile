$HTTPDUSER=`ps aux | grep -E '[a]pache|[h]ttpd|[_]www|[w]ww-data|[n]ginx' | grep -v root | head -1 | cut -d\  -f1`

clean:
	php app/console cache:clear

fix_permissions:
	sudo setfacl -R -m u:www-data:rwX -m u:`whoami`:rwX app/cache app/logs web/uploads
	sudo setfacl -dR -m u:www-data:rwX -m u:`whoami`:rwX app/cache app/logs web/uploads

	# chmod -R +a "www-data allow delete,write,append,file_inherit,directory_inherit" app/cache app/logs web/uploads
	# chmod -R +a "`whoami` allow delete,write,append,file_inherit,directory_inherit" app/cache app/logs web/uploads

install_assets:
	php app/console assets:install web --symlink

trans_extract:
	php app/console translation:extract ro --dir=./src/ --output-dir=./app/Resources/translations
	php app/console translation:extract en --dir=./src/ --output-dir=./app/Resources/translations

trans_export: 
	php app/console lexik:translations:export
 
trans_import: 
	php app/console lexik:translations:import

trans_generate: 
	make trans_extract 
	make trans_import

trans_save: 
	make trans_export
	make clean