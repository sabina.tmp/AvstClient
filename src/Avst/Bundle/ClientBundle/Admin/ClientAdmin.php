<?php

namespace Avst\Bundle\ClientBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ClientAdmin extends Admin
{
    protected $baseRoutePattern = '/client';

    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array(
                'label' => 'client.name.label',
                'translation_domain' => 'Avst',
            ))
            ->add('CUI', 'text', array(
                'label' => 'client.cui.label',
                'translation_domain' => 'Avst',
                // 'required' => true,
            ))
            ->add('address', 'text', array(
                'label' => 'client.address.label',
                'translation_domain' => 'Avst',
                // 'required' => false,
            ))
            ->add('registrationNumber', 'text', array(
                'label' => 'client.registration_no.label',
                'translation_domain' => 'Avst',
                'required' => true,
            ))
            ->add('accountNumber', 'text', array(
                'label' => 'client.account_no.label',
                'translation_domain' => 'Avst',
                'required' => false,
            ))
            ->add('bank', 'text', array(
                'label' => 'client.bank.label',
                'translation_domain' => 'Avst',
                'required' => false,
            ))
            ->add('notes', 'text', array(
                'label' => 'client.notes.label',
                'translation_domain' => 'Avst',
                'required' => false,
            ))
            ->add('contactPerson', 'sonata_type_collection', array(
                    'attr' => array('row_class' => ''),
                    'label' => 'client.contact_person.label',
                    'translation_domain' => 'Avst',
                    'required' => false,
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                )
            )
            ->add('contact', 'sonata_type_collection', array(
                    'attr' => array('row_class' => ''),
                    'label' => 'client.contact.label',
                    'translation_domain' => 'Avst',
                    'required' => false,
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                )
            )
            ->add('file', 'sonata_type_collection', array(
                    'attr' => array('row_class' => ''),
                    'label' => 'client.file.label',
                    'translation_domain' => 'Avst',
                    'required' => false,
                // 'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'
                ),
                array(
                    'edit' => 'inline',
                    'inline' => 'table',
                )
            )
        ;
    }

    // Fields to be shown on filter forms
    protected function configureDatagridFilters(DatagridMapper $datagridMapper)
    {
        $datagridMapper
            ->add('name', null, array(
                'label' => 'client.name.label',
                'translation_domain' => 'Avst',
            ))
            ->add('CUI', null, array(
                'label' => 'client.cui.label',
                'translation_domain' => 'Avst',
            ))
            ->add('address', null, array(
                'label' => 'client.address.label',
                'translation_domain' => 'Avst',
            ))
            ->add('contact.date', 'doctrine_orm_date_range', array(
                'field_type'=>'sonata_type_date_range',
                'label' => 'client.contact.label',
                'translation_domain' => 'Avst',
                'field_options'=> array(
                    'widget' => 'single_text', 
                    'required' => false, 
                    'attr' => array(
                        'class' => 'datepicker'
                    )
                )
            ))
            ->add('file.expiry', 'doctrine_orm_date_range', array(
                'field_type'=>'sonata_type_date_range',
                'label' => 'client.file.label',
                'translation_domain' => 'Avst',
                'field_options'=> array(
                    'widget' => 'single_text', 
                    'required' => false, 
                    'attr' => array(
                        'class' => 'datepicker'
                    )
                )
            ))
        ;
    }

    // Fields to be shown on lists
    protected function configureListFields(ListMapper $listMapper)
    {
        $listMapper
            ->addIdentifier('name', 'strig',  array(
                'label' => 'client.name.label',
                'translation_domain' => 'Avst',
            ))
            ->add('CUI', 'strig',  array(
                'label' => 'client.cui.label',
                'translation_domain' => 'Avst',
            ))
            ->add('address', 'strig',  array(
                'label' => 'client.address.label',
                'translation_domain' => 'Avst',
            ))
        ;
    }


    public function getExportFormats()
    {
        return array('xls');
    }

    public function getBatchActions()
    {
        return array();
    }

    public function prePersist($object) {
        $this->manageEmbeddedAdmins($object);
    }

    public function preUpdate($object) {
        $this->manageEmbeddedAdmins($object);
    }

    private function manageEmbeddedAdmins($object) {
        $contacts = $object->getContact();
        if ($contacts) {
            foreach ($contacts as $contact) {
                $contact->setClient($object);
            }
        }

        $files = $object->getFile();
        if ($files) {
            foreach ($files as $file) {
                $file->setClient($object);
            }
        }

        $contactPersons = $object->getContactPerson();
        if ($contactPersons) {
            foreach ($contactPersons as $file) {
                $file->setClient($object);
            }
        }

    }


}