<?php

namespace Avst\Bundle\ClientBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class FileAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('file', 'file', array(
                'label' => 'file.file.label',
                'translation_domain' => 'Avst',
                // 'template' => 'SonataMediaBundle:MediaAdmin:list_image.html.twig'
            ))
            ->add('type', 'choice', array(
                'label' => 'file.type.label',
                'translation_domain' => 'Avst',
                'choices' =>  [
                    'offer' => 'file.type.offer',
                    'contract' => 'file.type.contract'
                ],
                'required' => true,
            ))
            ->add('expiry', 'sonata_type_date_picker', array(
                'label' => 'file.expiry.label',
                'translation_domain' => 'Avst',
                'format' => 'dd.MM.y',
                'required' => false,
            ))
            ->add('value', 'text', array(
                'label' => 'file.value.label',
                'translation_domain' => 'Avst',
                'required' => false,
            ))
            ->add('notes', 'text', array(
                'label' => 'file.notes.label',
                'translation_domain' => 'Avst',
                'required' => false,
            ))
        ;
    }

    public function getExportFormats()
    {
        return array();
    }

    public function getBatchActions()
    {
        return array();
    }

}