<?php

namespace Avst\Bundle\ClientBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ContactDateAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('date', 'sonata_type_date_picker', array(
                'label' => 'contact.date.label',
                'translation_domain' => 'Avst',
                'format' => 'dd.MM.y',
            ))
            ->add('notes', 'text', array(
                'label' => 'contact.notes.label',
                'translation_domain' => 'Avst',
                'required' => false,
            ))
        ;
    }

    public function getExportFormats()
    {
        return array();
    }

    public function getBatchActions()
    {
        return array();
    }

}