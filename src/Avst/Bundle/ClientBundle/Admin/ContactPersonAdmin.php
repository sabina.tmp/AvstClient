<?php

namespace Avst\Bundle\ClientBundle\Admin;

use Sonata\AdminBundle\Admin\Admin;
use Sonata\AdminBundle\Datagrid\ListMapper;
use Sonata\AdminBundle\Datagrid\DatagridMapper;
use Sonata\AdminBundle\Form\FormMapper;

class ContactPersonAdmin extends Admin
{
    // Fields to be shown on create/edit forms
    protected function configureFormFields(FormMapper $formMapper)
    {
        $formMapper
            ->add('name', 'text', array(
                'label' => 'contact_person.name.label',
                'translation_domain' => 'Avst',
                'required' => true,
            ))
            ->add('email', 'text', array(
                'label' => 'contact_person.email.label',
                'translation_domain' => 'Avst',
                'required' => false,
            ))
            ->add('telephone', 'text', array(
                'label' => 'contact_person.telephone.label',
                'translation_domain' => 'Avst',
                'required' => false,
            ))
            ->add('fax', 'text', array(
                'label' => 'contact_person.fax.label',
                'translation_domain' => 'Avst',
                'required' => false,
            ))
            ->add('function', 'text', array(
                'label' => 'contact_person.function.label',
                'translation_domain' => 'Avst',
                'required' => true,
            ))
        ;
    }

    public function getExportFormats()
    {
        return array();
    }

    public function getBatchActions()
    {
        return array();
    }

}