<?php

namespace Avst\Bundle\ClientBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * @ORM\Entity
 * @ORM\Table(name="client",
 *      indexes={
 *          @ORM\Index(
 *              name="cui_idx",
 *              columns={"cui"}
 *          ),
 *          @ORM\Index(
 *              name="name_idx",
 *              columns={"name"}
 *          ),
 *          @ORM\Index(
 *              name="address_idx",
 *              columns={"address"}
 *          )
 *      },
 *      uniqueConstraints={
 *          @ORM\UniqueConstraint(
 *              columns={"cui", "cui"}
 *          )
 *      }
 * )
 */
class Client
{
    /**
     * @ORM\Id
     * @ORM\Column(type="integer")
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    protected $id;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $name;

    /**
     * @ORM\Column(type="string", length=255)
     */
    protected $address;

    /**
     * @ORM\Column(type="string", name="registration_no", length=100)
     */
    protected $registrationNumber;

    /**
     * @ORM\Column(type="string", name="account_no", length=100, nullable=true)
     */
    protected $accountNumber;

        /**
     * @ORM\Column(type="string", name="bank", length=100, nullable=true)
     */
    protected $bank;

    /**
     * @ORM\Column(type="string", name="notes", length=10000, nullable=true)
     */
    protected $notes;

    /**
     * @ORM\Column(type="string", name="cui", length=50)
     */
    protected $CUI;

    /**
     * @ORM\OneToMany(targetEntity="ContactDate", mappedBy="client", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $contact;

    /**
     * @ORM\OneToMany(targetEntity="File", mappedBy="client", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $file;

    /**
     * @ORM\OneToMany(targetEntity="ContactPerson", mappedBy="client", cascade={"persist", "remove"}, orphanRemoval=true)
     */
    protected $contactPerson;

    /**
     * Get id
     *
     * @return integer
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set name
     *
     * @param string $name
     *
     * @return Client
     */
    public function setName($name)
    {
        $this->name = $name;

        return $this;
    }

    /**
     * Get name
     *
     * @return string
     */
    public function getName()
    {
        return $this->name;
    }

    /**
     * Set address
     *
     * @param string $address
     *
     * @return Client
     */
    public function setAddress($address)
    {
        $this->address = $address;

        return $this;
    }

    /**
     * Get address
     *
     * @return string
     */
    public function getAddress()
    {
        return $this->address;
    }

    /**
     * Set email
     *
     * @param string $email
     *
     * @return Client
     */
    public function setEmail($email)
    {
        $this->email = $email;

        return $this;
    }

    /**
     * Get email
     *
     * @return string
     */
    public function getEmail()
    {
        return $this->email;
    }

    /**
     * Set cUI
     *
     * @param string $cUI
     *
     * @return Client
     */
    public function setCUI($cUI)
    {
        $this->CUI = $cUI;

        return $this;
    }

    /**
     * Get cUI
     *
     * @return string
     */
    public function getCUI()
    {
        return $this->CUI;
    }

    public function __toString()
    {
        if ($name = $this->getName()) {
            return $name;
        }
        return '';
    }

    /**
     * Constructor
     */
    public function __construct()
    {
        $this->contact = new \Doctrine\Common\Collections\ArrayCollection();
    }

    /**
     * Add contact
     *
     * @param \Avst\Bundle\ClientBundle\Entity\ContactDate $contact
     *
     * @return Client
     */
    public function addContact(\Avst\Bundle\ClientBundle\Entity\ContactDate $contact)
    {
        $this->contact[] = $contact;

        return $this;
    }

    /**
     * Remove contact
     *
     * @param \Avst\Bundle\ClientBundle\Entity\ContactDate $contact
     */
    public function removeContact(\Avst\Bundle\ClientBundle\Entity\ContactDate $contact)
    {
        $this->contact->removeElement($contact);
    }

    /**
     * Get contact
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContact()
    {
        return $this->contact;
    }

    /**
     * Add file
     *
     * @param \Avst\Bundle\ClientBundle\Entity\File $file
     *
     * @return Client
     */
    public function addFile(\Avst\Bundle\ClientBundle\Entity\File $file)
    {
        $this->file[] = $file;

        return $this;
    }

    /**
     * Remove file
     *
     * @param \Avst\Bundle\ClientBundle\Entity\File $file
     */
    public function removeFile(\Avst\Bundle\ClientBundle\Entity\File $file)
    {
        $this->file->removeElement($file);
    }

    /**
     * Get file
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getFile()
    {
        return $this->file;
    }

    /**
     * Set registrationNumber
     *
     * @param string $registrationNumber
     *
     * @return Client
     */
    public function setRegistrationNumber($registrationNumber)
    {
        $this->registrationNumber = $registrationNumber;

        return $this;
    }

    /**
     * Get registrationNumber
     *
     * @return string
     */
    public function getRegistrationNumber()
    {
        return $this->registrationNumber;
    }

    /**
     * Set accountNumber
     *
     * @param string $accountNumber
     *
     * @return Client
     */
    public function setAccountNumber($accountNumber)
    {
        $this->accountNumber = $accountNumber;

        return $this;
    }

    /**
     * Get accountNumber
     *
     * @return string
     */
    public function getAccountNumber()
    {
        return $this->accountNumber;
    }

    /**
     * Set bank
     *
     * @param string $bank
     *
     * @return Client
     */
    public function setBank($bank)
    {
        $this->bank = $bank;

        return $this;
    }

    /**
     * Get bank
     *
     * @return string
     */
    public function getBank()
    {
        return $this->bank;
    }

    /**
     * Set notes
     *
     * @param string $notes
     *
     * @return Client
     */
    public function setNotes($notes)
    {
        $this->notes = $notes;

        return $this;
    }

    /**
     * Get notes
     *
     * @return string
     */
    public function getNotes()
    {
        return $this->notes;
    }

    /**
     * Add contactPerson
     *
     * @param \Avst\Bundle\ClientBundle\Entity\ContactPerson $contactPerson
     *
     * @return Client
     */
    public function addContactPerson(\Avst\Bundle\ClientBundle\Entity\ContactPerson $contactPerson)
    {
        $this->contactPerson[] = $contactPerson;

        return $this;
    }

    /**
     * Remove contactPerson
     *
     * @param \Avst\Bundle\ClientBundle\Entity\ContactPerson $contactPerson
     */
    public function removeContactPerson(\Avst\Bundle\ClientBundle\Entity\ContactPerson $contactPerson)
    {
        $this->contactPerson->removeElement($contactPerson);
    }

    /**
     * Get contactPerson
     *
     * @return \Doctrine\Common\Collections\Collection
     */
    public function getContactPerson()
    {
        return $this->contactPerson;
    }
}
