<?php

namespace Avst\Bundle\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class CalendarController extends Controller
{
	/**
	 * @Template()
	 * @Route("/admin/calendar/", name="avst_calendar")
	 */
    public function indexAction()
    {
        return array();
    }
}
