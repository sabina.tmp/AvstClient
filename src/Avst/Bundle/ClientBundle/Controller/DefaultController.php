<?php

namespace Avst\Bundle\ClientBundle\Controller;

use Symfony\Bundle\FrameworkBundle\Controller\Controller;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Template;
use Sensio\Bundle\FrameworkExtraBundle\Configuration\Route;

class DefaultController extends Controller
{
	/**
	 * @Template()
	 * @Route("/")
	 */
    public function indexAction()
    {
    	return $this->redirectToRoute('admin_avst_client_client_list', array(), 301);
    }
}
